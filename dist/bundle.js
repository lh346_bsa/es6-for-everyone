/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/javascript/app */ "./src/javascript/app.js");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./src/styles/styles.css */ "./src/styles/styles.css");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__);


Object(_src_javascript_app__WEBPACK_IMPORTED_MODULE_0__["startApp"])();

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css":
/*!*********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "html,\nbody {\n    height: 100%;\n    width: 100%;\n    margin: 0;\n    padding: 0;\n}\n\n#root {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    height: 100%;\n    width: 100%;\n}\n\n.fighters {\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    flex: 1;\n    flex-wrap: wrap;\n    padding: 0 15px;\n}\n\n.fighter {\n    display: flex;\n    flex-direction: column;\n    padding: 20px;\n}\n\n.fighter:hover {\n    box-shadow: 0 0 50px 10px rgba(0,0,0,0.06);\n    cursor: pointer;\n}\n\n.name {\n    align-self: center;\n    font-size: 21px;\n    margin-top: 20px;\n}\n\n.fighter-image {\n    height: 260px;\n}\n\n#loading-overlay {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    font-size: 18px;\n    background: rgba(255, 255, 255, 0.7);\n    visibility: hidden;\n}\n\n.modal-layer {\n    position: absolute;\n    top: 0;\n    left: 0;\n    height: 100%;\n    width: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    background-color: rgba(128,128,128,0.6);\n}\n\n.modal-root {\n    display: flex;\n    flex-direction: column;\n    border: 1px solid rgba(0,0,0,.2);\n    border-radius: .3rem;\n    background-color: white;\n}\n\n.modal-header {\n    display: flex;\n    align-items: center;\n    justify-content: space-between;\n    min-width: 300px;\n    padding: 1rem;\n    border-bottom: 1px solid #e9ecef;\n    border-top-left-radius: .3rem;\n    border-top-right-radius: .3rem;\n    font-weight: 700;\n    font-size: 22px;\n}\n\n.modal-body {\n    padding: 1rem;\n}\n\n.close-btn {\n    font-size: 1.5rem;\n    font-weight: 700;\n    line-height: 1;\n    cursor: pointer;\n}\n\n.custom-checkbox {\n    display: block;\n    position: relative;\n    padding-left: 35px;\n    margin-bottom: 12px;\n    cursor: pointer;\n    font-size: 22px;\n    -webkit-user-select: none;\n    -moz-user-select: none;\n    -ms-user-select: none;\n    user-select: none;\n}\n  \n.custom-checkbox input {\n    position: absolute;\n    opacity: 0;\n    cursor: pointer;\n    height: 0;\n    width: 0;\n}\n  \n.checkmark {\n    position: absolute;\n    top: 0;\n    left: 0;\n    height: 25px;\n    width: 25px;\n    background-color: #eee;\n}\n  \n.custom-checkbox:hover input ~ .checkmark {\n    background-color: #ccc;\n}\n  \n.custom-checkbox input:checked ~ .checkmark {\n    background-color: #2196F3;\n}\n  \n.checkmark:after {\n    content: \"\";\n    position: absolute;\n    display: none;\n}\n  \n.custom-checkbox input:checked ~ .checkmark:after {\n    display: block;\n}\n\n.custom-checkbox .checkmark:after {\n    left: 9px;\n    top: 5px;\n    width: 5px;\n    height: 10px;\n    border: solid white;\n    border-width: 0 3px 3px 0;\n    -webkit-transform: rotate(45deg);\n    -ms-transform: rotate(45deg);\n    transform: rotate(45deg);\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./src/javascript/app.js":
/*!*******************************!*\
  !*** ./src/javascript/app.js ***!
  \*******************************/
/*! exports provided: startApp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startApp", function() { return startApp; });
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./services/fightersService */ "./src/javascript/services/fightersService.js");
/* harmony import */ var _fightersView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fightersView */ "./src/javascript/fightersView.js");


const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
async function startApp() {
  try {
    loadingElement.style.visibility = 'visible';
    const fighters = await Object(_services_fightersService__WEBPACK_IMPORTED_MODULE_0__["getFighters"])();
    const fightersElement = Object(_fightersView__WEBPACK_IMPORTED_MODULE_1__["createFighters"])(fighters);
    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}

/***/ }),

/***/ "./src/javascript/fight.js":
/*!*********************************!*\
  !*** ./src/javascript/fight.js ***!
  \*********************************/
/*! exports provided: fight, getDamage, getHitPower, getBlockPower */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fight", function() { return fight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getDamage", function() { return getDamage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHitPower", function() { return getHitPower; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBlockPower", function() { return getBlockPower; });
function fight(firstFighter, secondFighter) {// return winner
}
function getDamage(attacker, enemy) {// damage = hit - block
  // return damage 
}
function getHitPower(fighter) {// return hit power
}
function getBlockPower(fighter) {// return block power
}

/***/ }),

/***/ "./src/javascript/fighterView.js":
/*!***************************************!*\
  !*** ./src/javascript/fighterView.js ***!
  \***************************************/
/*! exports provided: createFighter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createFighter", function() { return createFighter; });
/* harmony import */ var _helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helpers/domHelper */ "./src/javascript/helpers/domHelper.js");

function createFighter(fighter, handleClick, selectFighter) {
  const {
    name,
    source
  } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox(source);
  const fighterContainer = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'div',
    className: 'fighter'
  });
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = ev => ev.stopPropagation();

  const onCheckboxClick = ev => selectFighter(ev, fighter);

  const onFighterClick = ev => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick, false);
  return fighterContainer;
}

function createName(name) {
  const nameElement = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'span',
    className: 'name'
  });
  nameElement.innerText = name;
  return nameElement;
}

function createImage(source) {
  const attributes = {
    src: source
  };
  const imgElement = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'img',
    className: 'fighter-image',
    attributes
  });
  return imgElement;
}

function createCheckbox() {
  const label = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'label',
    className: 'custom-checkbox'
  });
  const span = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'span',
    className: 'checkmark'
  });
  const attributes = {
    type: 'checkbox'
  };
  const checkboxElement = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'input',
    attributes
  });
  label.append(checkboxElement, span);
  return label;
}

/***/ }),

/***/ "./src/javascript/fightersView.js":
/*!****************************************!*\
  !*** ./src/javascript/fightersView.js ***!
  \****************************************/
/*! exports provided: createFighters, getFighterInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createFighters", function() { return createFighters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFighterInfo", function() { return getFighterInfo; });
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fighterView */ "./src/javascript/fighterView.js");
/* harmony import */ var _modals_fighterDetails__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modals/fighterDetails */ "./src/javascript/modals/fighterDetails.js");
/* harmony import */ var _helpers_domHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./helpers/domHelper */ "./src/javascript/helpers/domHelper.js");
/* harmony import */ var _fight__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fight */ "./src/javascript/fight.js");
/* harmony import */ var _modals_winner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modals/winner */ "./src/javascript/modals/winner.js");





function createFighters(fighters) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => Object(_fighterView__WEBPACK_IMPORTED_MODULE_0__["createFighter"])(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_2__["createElement"])({
    tagName: 'div',
    className: 'fighters'
  });
  fightersContainer.append(...fighterElements);
  return fightersContainer;
}
const fightersDetailsCache = new Map();

async function showFighterDetails(event, fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  Object(_modals_fighterDetails__WEBPACK_IMPORTED_MODULE_1__["showFighterDetailsModal"])(fullInfo);
}

async function getFighterInfo(fighterId) {// get fighter form fightersDetailsCache or use getFighterDetails function
}

function createFightersSelector() {
  const selectedFighters = new Map();
  return async function selectFighterForBattle(event, fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const winner = Object(_fight__WEBPACK_IMPORTED_MODULE_3__["fight"])(...selectedFighters.values());
      Object(_modals_winner__WEBPACK_IMPORTED_MODULE_4__["showWinnerModal"])(winner);
    }
  };
}

/***/ }),

/***/ "./src/javascript/helpers/apiHelper.js":
/*!*********************************************!*\
  !*** ./src/javascript/helpers/apiHelper.js ***!
  \*********************************************/
/*! exports provided: callApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callApi", function() { return callApi; });
/* harmony import */ var _mockData__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mockData */ "./src/javascript/helpers/mockData.js");

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;

async function callApi(endpoint, method) {
  const url = API_URL + endpoint;
  const options = {
    method
  };
  return useMockAPI ? fakeCallApi(endpoint) : fetch(url, options).then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load'))).then(result => JSON.parse(atob(result.content))).catch(error => {
    throw error;
  });
}

async function fakeCallApi(endpoint) {
  const response = endpoint === 'fighters.json' ? _mockData__WEBPACK_IMPORTED_MODULE_0__["fighters"] : getFighterById(endpoint);
  return new Promise((resolve, reject) => {
    setTimeout(() => response ? resolve(response) : reject(Error('Failed to load')), 500);
  });
}

function getFighterById(endpoint) {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);
  return _mockData__WEBPACK_IMPORTED_MODULE_0__["fightersDetails"].find(it => it._id === id);
}



/***/ }),

/***/ "./src/javascript/helpers/domHelper.js":
/*!*********************************************!*\
  !*** ./src/javascript/helpers/domHelper.js ***!
  \*********************************************/
/*! exports provided: createElement */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createElement", function() { return createElement; });
function createElement({
  tagName,
  className,
  attributes = {}
}) {
  const element = document.createElement(tagName);

  if (className) {
    element.classList.add(className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  return element;
}

/***/ }),

/***/ "./src/javascript/helpers/mockData.js":
/*!********************************************!*\
  !*** ./src/javascript/helpers/mockData.js ***!
  \********************************************/
/*! exports provided: fighters, fightersDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fighters", function() { return fighters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fightersDetails", function() { return fightersDetails; });
const fighters = [{
  "_id": "1",
  "name": "Ryu",
  "source": "https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif"
}, {
  "_id": "2",
  "name": "Dhalsim",
  "source": "https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif"
}, {
  "_id": "3",
  "name": "Guile",
  "source": "https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif"
}, {
  "_id": "4",
  "name": "Zangief",
  "source": "https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif"
}, {
  "_id": "5",
  "name": "Ken",
  "source": "https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif"
}, {
  "_id": "6",
  "name": "Bison",
  "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
}];
const fightersDetails = [{
  "_id": "1",
  "name": "Ryu",
  "health": 45,
  "attack": 4,
  "defense": 3,
  "source": "https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif"
}, {
  "_id": "2",
  "name": "Dhalsim",
  "health": 60,
  "attack": 3,
  "defense": 1,
  "source": "https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif"
}, {
  "_id": "3",
  "name": "Guile",
  "health": 45,
  "attack": 4,
  "defense": 3,
  "source": "https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif"
}, {
  "_id": "4",
  "name": "Zangief",
  "health": 60,
  "attack": 4,
  "defense": 1,
  "source": "https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif"
}, {
  "_id": "5",
  "name": "Ken",
  "health": 45,
  "attack": 3,
  "defense": 4,
  "source": "https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif"
}, {
  "_id": "6",
  "name": "Bison",
  "health": 45,
  "attack": 5,
  "defense": 4,
  "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
}];

/***/ }),

/***/ "./src/javascript/modals/fighterDetails.js":
/*!*************************************************!*\
  !*** ./src/javascript/modals/fighterDetails.js ***!
  \*************************************************/
/*! exports provided: showFighterDetailsModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showFighterDetailsModal", function() { return showFighterDetailsModal; });
/* harmony import */ var _helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/domHelper */ "./src/javascript/helpers/domHelper.js");
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modal */ "./src/javascript/modals/modal.js");


function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  Object(_modal__WEBPACK_IMPORTED_MODULE_1__["showModal"])({
    title,
    bodyElement
  });
}

function createFighterDetails(fighter) {
  const {
    name
  } = fighter;
  const fighterDetails = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'div',
    className: 'modal-body'
  });
  const nameElement = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'span',
    className: 'fighter-name'
  }); // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  fighterDetails.append(nameElement);
  return fighterDetails;
}

/***/ }),

/***/ "./src/javascript/modals/modal.js":
/*!****************************************!*\
  !*** ./src/javascript/modals/modal.js ***!
  \****************************************/
/*! exports provided: showModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showModal", function() { return showModal; });
/* harmony import */ var _helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/domHelper */ "./src/javascript/helpers/domHelper.js");

function showModal({
  title,
  bodyElement
}) {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement);
  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal(title, bodyElement) {
  const layer = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'div',
    className: 'modal-layer'
  });
  const modalContainer = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'div',
    className: 'modal-root'
  });
  const header = createHeader(title);
  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);
  return layer;
}

function createHeader(title) {
  const headerElement = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'div',
    className: 'modal-header'
  });
  const titleElement = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'span'
  });
  const closeButton = Object(_helpers_domHelper__WEBPACK_IMPORTED_MODULE_0__["createElement"])({
    tagName: 'div',
    className: 'close-btn'
  });
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  return headerElement;
}

function hideModal(event) {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal === null || modal === void 0 ? void 0 : modal.remove();
}

/***/ }),

/***/ "./src/javascript/modals/winner.js":
/*!*****************************************!*\
  !*** ./src/javascript/modals/winner.js ***!
  \*****************************************/
/*! exports provided: showWinnerModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showWinnerModal", function() { return showWinnerModal; });
function showWinnerModal(fighter) {// show winner name and image
}

/***/ }),

/***/ "./src/javascript/services/fightersService.js":
/*!****************************************************!*\
  !*** ./src/javascript/services/fightersService.js ***!
  \****************************************************/
/*! exports provided: getFighters, getFighterDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFighters", function() { return getFighters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFighterDetails", function() { return getFighterDetails; });
/* harmony import */ var _helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/apiHelper */ "./src/javascript/helpers/apiHelper.js");

async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, 'GET');
    return apiResult;
  } catch (error) {
    throw error;
  }
}
async function getFighterDetails(id) {// endpoint - `details/fighter/${id}.json`;
}

/***/ }),

/***/ "./src/styles/styles.css":
/*!*******************************!*\
  !*** ./src/styles/styles.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./styles.css */ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map