export function fight(firstFighter, secondFighter) {
  // return winner
    let {health: healthFirst} = firstFighter;
    let {health: healthSecond} = secondFighter;

    while (true) {
        healthSecond -= getDamage(firstFighter, secondFighter);
        if (healthSecond <= 0) {
            return firstFighter;
        }
        healthFirst -= getDamage(secondFighter, firstFighter);
        if (healthFirst <= 0) {
            return secondFighter;
        }
    }
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(enemy);
    let damage = hitPower - blockPower;
  // return damage
    if (damage < 0) {
        damage = 0
    }
    return damage;
}

export function getHitPower(fighter) {
  // return hit power
    const {attack} = fighter
    const criticalHitChance = Math.random() + 1;
    const power = attack * criticalHitChance;
    return power;
}

export function getBlockPower(fighter) {
  // return block power
    const {defense} = fighter;
    const dodgeChance = Math.random() + 1;
    const power = defense * dodgeChance;
    return power;
}
